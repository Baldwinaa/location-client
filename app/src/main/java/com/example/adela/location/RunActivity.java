package com.example.adela.location;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class RunActivity extends AppCompatActivity {
    private Intent service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        service = new Intent(getApplicationContext(), ServiceGPS.class);
        setContentView(R.layout.activity_run);
        startService(service);
    }
    @Override
    public void onStop() {
        super.onStop();
        stopService(service);
    }
}
