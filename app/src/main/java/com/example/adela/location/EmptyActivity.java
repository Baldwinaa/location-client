package com.example.adela.location;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class EmptyActivity extends AppCompatActivity {

    private SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        preferences = getSharedPreferences(getString(R.string.identifier), Activity.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);
    }

    public void onClickLogoutButton(View view){
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.putString(getString(R.string.identifier), "");
        preferencesEditor.commit();

        Intent myIntent = new Intent(EmptyActivity.this, LoginActivity.class);
        EmptyActivity.this.startActivity(myIntent);
    }
}
