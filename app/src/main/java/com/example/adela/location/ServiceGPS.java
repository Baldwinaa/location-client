package com.example.adela.location;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

public class ServiceGPS extends Service {

    public ServiceGPS() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast toast = Toast.makeText(getApplicationContext(), "START SERVICE !", Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast toast = Toast.makeText(getApplicationContext(), "STOOP !", Toast.LENGTH_SHORT);
        toast.show();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}