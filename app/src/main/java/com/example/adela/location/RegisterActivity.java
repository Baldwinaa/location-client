package com.example.adela.location;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    String result;
    JSONObject jsonObject = new JSONObject();
    private SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        preferences = getSharedPreferences(getString(R.string.identifier), Activity.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void onClickRegisterButton(View view)
    {
        if (new ConnectivityChecker().isNetworkAvailable(getApplicationContext())) {

            EditText login = (EditText) findViewById(R.id.editTextLogin);
            EditText password = (EditText) findViewById(R.id.editTextPassword);
            login.setError(null);
            password.setError(null);

            if(correctData(login, password)) {
                try {
                    jsonObject = new JSONObject()
                            .put("nickname", login.getText())
                            .put("password", password.getText());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                final String jsonString = jsonObject.toString();
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        result = Http.Post(getString(R.string.ip_url) + getString(R.string.registration_url), jsonString);
                    }
                });
                thread.start();
                try {
                    thread.join();
                    JSONObject jsonResult = new JSONObject(result.toString());
                    JSONObject json = new JSONObject(jsonResult.get("newUser").toString());
                    saveData(json.get("identifier").toString());
                    Intent myIntent = new Intent(RegisterActivity.this, EmptyActivity.class);
                    RegisterActivity.this.startActivity(myIntent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            Toast.makeText(getApplicationContext(), getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
        }
    }

    private void saveData(String identifier) {
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.putString(getString(R.string.identifier), identifier);
        preferencesEditor.commit();
    }
    private boolean correctData(EditText login, EditText password){
        if(login.getText().toString().equals("") && password.getText().toString().equals(""))
        {
            login.setError(getString(R.string.error_field_required));
            password.setError(getString(R.string.error_field_required));
            return false;
        }
        else if(login.getText().toString().equals(""))
        {
            login.setError(getString(R.string.error_field_required));
            return false;
        }
        else if(password.getText().toString().equals(""))
        {
            password.setError(getString(R.string.error_field_required));
            return false;
        }
        else if(password.getText().toString().length()<5){
            password.setError(getString(R.string.password_length_required));
            return false;
        }
        else if(!checkString(password.getText().toString()))
        {
            password.setError(getString(R.string.must_contain_required));
            return false;
        }
        return true;
    }

    private static boolean checkString(String str) {
        char ch;
        boolean capitalFlag = false;
        boolean lowerCaseFlag = false;
        boolean numberFlag = false;
        for(int i=0;i < str.length();i++) {
            ch = str.charAt(i);
            if( Character.isDigit(ch)) {
                numberFlag = true;
            }
            else if (Character.isUpperCase(ch)) {
                capitalFlag = true;
            } else if (Character.isLowerCase(ch)) {
                lowerCaseFlag = true;
            }
            if(numberFlag && capitalFlag && lowerCaseFlag)
                return true;
        }
        return false;
    }
}

