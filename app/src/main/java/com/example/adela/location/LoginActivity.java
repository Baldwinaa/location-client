package com.example.adela.location;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private SharedPreferences preferences;
    JSONObject jsonObject;
    String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        preferences = getSharedPreferences(getString(R.string.identifier), Activity.MODE_PRIVATE);
        if(!getIdentifier().equals(""))
        {
            Intent myIntent = new Intent(LoginActivity.this, EmptyActivity.class);
            LoginActivity.this.startActivity(myIntent);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

    }


    public void onClickRegisterButton(View view)
    {
        Intent myIntent = new Intent(LoginActivity.this, RegisterActivity.class);
        LoginActivity.this.startActivity(myIntent);

    }

    public void onClickLoginButton(View view)
    {
        if (new ConnectivityChecker().isNetworkAvailable(getApplicationContext())) {
            EditText login = (EditText) findViewById(R.id.editTextLogin);
            EditText password = (EditText) findViewById(R.id.editTextPassword);
            try {
                jsonObject = new JSONObject()
                        .put("nickname", login.getText())
                        .put("password", password.getText());


            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String  jsonString = jsonObject.toString();
            Thread thread = new Thread(new Runnable(){
                @Override
                public void run() {
                    result = Http.Post(getString(R.string.ip_url)+getString(R.string.login_url), jsonString);
                }
            });
            thread.start();
            /*try {
                thread.join();
                JSONObject jsonResult = new JSONObject(result.toString());
                JSONObject json = new JSONObject(jsonResult.get("user").toString());
                saveData(json.get("identifier").toString());
                Intent myIntent = new Intent(LoginActivity.this, EmptyActivity.class);
                LoginActivity.this.startActivity(myIntent);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
        }
        else {
            Toast.makeText(getApplicationContext(), getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
        }
    }

    private void saveData(String identifier) {
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.putString(getString(R.string.identifier), identifier);
        preferencesEditor.commit();
    }

    private String getIdentifier() {
        return preferences.getString(getString(R.string.identifier), "");
    }
}
